### What is this repository for? ###

* Shopping cart App
* Coding Challenge Version: efa33c904451d47e742d4236c72e36cdd157b354

### How do I get set up? ###

* Install maven and Java11 if not installed before
* mvn clean test
* ShoppingCartTest is starting point

### Who do I talk to? ###

* selvakumar.ponnusamy@gmail.com