package com.codingchallenge.shoppingcart;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.math.BigDecimal;

import org.junit.Test;

public class ProductTest {
    @Test
    public void testEquals() {
        Product p1 = new Product("Mysore Sandal Soap", BigDecimal.valueOf(49.99));
        Product p2 = p1;

        assertEquals(p1, p2);
    }

    @Test
    public void testNotEquals_1() {
        Product p1 = new Product("Mysore Sandal Soap", BigDecimal.valueOf(49.99));
        Product p2 = new Product("Mysore Sandal Soap", BigDecimal.valueOf(59.99));

        assertNotEquals(p1, p2);
    }

    @Test
    public void testNotEquals_2() {
        Product p1 = new Product("Mysore Sandal Soap", BigDecimal.valueOf(49.99));

        assertNotEquals(p1, null);
    }

    @Test
    public void testNotEquals_3() {
        Product p1 = new Product("Mysore Sandal Soap", BigDecimal.valueOf(49.99));

        assertNotEquals(p1, new Object());
    }
}
