package com.codingchallenge.shoppingcart.tax;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import org.junit.Test;

public class TaxCalculatorTest {

    @Test
    public void testTaxCalculator() {
        // given
        Tax salesTax = new SalesTax(12.5f);
        Tax addTax = (price) -> BigDecimal.valueOf(1.00);
        ITaxCalculator taxCalculator = new TaxCalculator(List.of(salesTax, addTax));

        // when
        BigDecimal tax = taxCalculator.compute(BigDecimal.valueOf(10));

        // then
        assertEquals(0, tax.setScale(2, RoundingMode.HALF_UP).compareTo(new BigDecimal("2.25")));

    }

}
