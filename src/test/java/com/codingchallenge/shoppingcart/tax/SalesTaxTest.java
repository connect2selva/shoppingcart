package com.codingchallenge.shoppingcart.tax;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.junit.Test;

public class SalesTaxTest {
    @Test
    public void testSalesTax() {
        // given
        Tax salesTax = new SalesTax(2.5f);

        // when
        BigDecimal tax = salesTax.calculateTax(BigDecimal.valueOf(10));

        // then
        assertEquals(0, tax.setScale(2, RoundingMode.HALF_UP).compareTo(new BigDecimal(".25")));
    }
}
