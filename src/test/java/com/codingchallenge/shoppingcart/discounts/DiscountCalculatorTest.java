package com.codingchallenge.shoppingcart.discounts;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.codingchallenge.shoppingcart.CartItem;
import com.codingchallenge.shoppingcart.Product;
import com.codingchallenge.shoppingcart.ShoppingCart;

public class DiscountCalculatorTest {
    @Test
    public void testComputeCartItemDiscounts() {
        // given
        Product doveSoap = new Product("Dove Soap", BigDecimal.valueOf(39.99));
        Map<Product, List<ICartItemDiscount>> discountsByProduct = Map.of(doveSoap,
                List.of(new BuyXGetYDiscount(2, 1)));
        IDiscountCalculator discountCalculator = new DiscountCalculator(discountsByProduct);
        com.codingchallenge.shoppingcart.ShoppingCart cart = new ShoppingCart();
        cart.addItem(new CartItem(doveSoap, 3));

        // when
        BigDecimal discounts = discountCalculator.compute(cart);

        // then
        assertEquals(new BigDecimal("39.99"), discounts);
    }

    @Test
    public void testComputeCartItemDiscounts_NoDiscount() {
        // given
        Product doveSoap = new Product("Dove Soap", BigDecimal.valueOf(39.99));
        Map<Product, List<ICartItemDiscount>> discountsByProduct = Map.of(doveSoap,
                List.of(new BuyXGetYDiscount(2, 1)));
        IDiscountCalculator discountCalculator = new DiscountCalculator(discountsByProduct);
        com.codingchallenge.shoppingcart.ShoppingCart cart = new ShoppingCart();
        cart.addItem(new CartItem(doveSoap, 1));

        // when
        BigDecimal discounts = discountCalculator.compute(cart);

        // then
        assertEquals(new BigDecimal("0.00"), discounts);
    }

    @Test
    public void testComputeCartItemDiscounts_MultipleDiscounts() {
        // given
        Product doveSoap = new Product("Dove Soap", BigDecimal.valueOf(39.99));
        ICartItemDiscount buyxGetY = new BuyXGetYDiscount(2, 1);
        ICartItemDiscount flatDiscount = (cartItem) -> {
            BigDecimal discountAmt = cartItem.getSalePrice().multiply(new BigDecimal("0.02"));
            cartItem.setSalePrice(discountAmt);
            return discountAmt;
        };

        Map<Product, List<ICartItemDiscount>> discountsByProduct = Map.of(doveSoap, List.of(buyxGetY,flatDiscount));
        IDiscountCalculator discountCalculator = new DiscountCalculator(discountsByProduct);
        com.codingchallenge.shoppingcart.ShoppingCart cart = new ShoppingCart();
        cart.addItem(new CartItem(doveSoap, 3));

        // when
        BigDecimal discounts = discountCalculator.compute(cart);

        // then
        assertEquals(new BigDecimal("41.59"), discounts.setScale(2, RoundingMode.HALF_UP));
    }
}
