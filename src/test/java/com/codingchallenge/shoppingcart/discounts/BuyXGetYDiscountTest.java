package com.codingchallenge.shoppingcart.discounts;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.Test;

import com.codingchallenge.shoppingcart.CartItem;
import com.codingchallenge.shoppingcart.Product;

public class BuyXGetYDiscountTest {
    @Test
    public void testBuyXGetY() {
        // given
        BuyXGetYDiscount discout = new BuyXGetYDiscount(2, 1);
        Product p1 = new Product("Soap", new BigDecimal("9.99"));

        // when
        BigDecimal discountVal1 = discout.applyAndGetDiscount(new CartItem(p1, 3));
        BigDecimal discountVal2 = discout.applyAndGetDiscount(new CartItem(p1, 8));

        // then
        assertEquals(new BigDecimal("9.99"), discountVal1);
        assertEquals(new BigDecimal("9.99").multiply(BigDecimal.valueOf(2)), discountVal2);
    }

    @Test
    public void testBuyXGetY_NoDiscount() {
        // given
        BuyXGetYDiscount discout = new BuyXGetYDiscount(2, 1);
        Product p1 = new Product("Soap", new BigDecimal("9.99"));

        // when
        BigDecimal discountVal1 = discout.applyAndGetDiscount(new CartItem(p1, 1));

        // then
        assertTrue(new BigDecimal("0.0").compareTo(discountVal1) == 0);
    }
}
