package com.codingchallenge.shoppingcart;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.codingchallenge.shoppingcart.discounts.BuyXGetYDiscount;
import com.codingchallenge.shoppingcart.discounts.DiscountCalculator;
import com.codingchallenge.shoppingcart.discounts.ICartItemDiscount;
import com.codingchallenge.shoppingcart.tax.SalesTax;
import com.codingchallenge.shoppingcart.tax.TaxCalculator;

public class ShoppingCartTest {
    @Test
    public void testEmptyShoppingCart() {
        ShoppingCart cart = new ShoppingCart();

        assertEquals(new BigDecimal("0.00"), cart.getTotalPrice());

    }

    @Test
    public void testAddProducts() {
        // Given
        ShoppingCart cart = new ShoppingCart();
        Product doveSoap = new Product("Dove Soap", BigDecimal.valueOf(39.99));

        CartItem cartItem = new CartItem(doveSoap, 5);

        // when
        cart.addItem(cartItem);

        // then
        assertEquals(5, cart.getItems().stream().findFirst().get().getQuantity());
        assertTrue(cart.getItems().stream().allMatch(item -> item.getProduct().getPrice() == doveSoap.getPrice()));
        assertEquals(new BigDecimal("199.95"), cart.getTotalPrice());

    }

    @Test
    public void testAddProducts_SameType() {
        // Given
        ShoppingCart cart = new ShoppingCart();
        Product doveSoap = new Product("Dove Soap", BigDecimal.valueOf(39.99));

        // when
        cart.addItem(new CartItem(doveSoap, 5));
        cart.addItem(new CartItem(doveSoap, 3));

        // then
        assertEquals(8, cart.getItems().stream().findFirst().get().getQuantity());
        assertTrue(cart.getItems().stream().allMatch(item -> item.getProduct().getPrice() == doveSoap.getPrice()));
        assertEquals(new BigDecimal("319.92"), cart.getTotalPrice());

    }

    ////////////////// Step 2 Starts ///////////////////
    @Test
    public void testAddProducts_SalesTax() {
        // Given
        ShoppingCart cart = new ShoppingCart();
        Product doveSoap = new Product("Dove Soap", BigDecimal.valueOf(39.99));
        Product axeDeos = new Product("Axe Deo", BigDecimal.valueOf(99.99));
        cart.setTaxCalculator(new TaxCalculator(List.of(new SalesTax(12.5f))));

        // when
        cart.addItem(new CartItem(doveSoap, 2));
        cart.addItem(new CartItem(axeDeos, 2));

        // then

        assertEquals(2,
                cart.getItems().stream().filter(i -> i.getProduct().equals(axeDeos)).findFirst().get().getQuantity());
        assertTrue(cart.getItems().stream().filter(i -> i.getProduct().equals(axeDeos))
                .allMatch(item -> item.getProduct().getPrice() == axeDeos.getPrice()));

        assertEquals(2,
                cart.getItems().stream().filter(i -> i.getProduct().equals(doveSoap)).findFirst().get().getQuantity());
        assertTrue(cart.getItems().stream().filter(i -> i.getProduct().equals(doveSoap))
                .allMatch(item -> item.getProduct().getPrice() == doveSoap.getPrice()));

        assertEquals(new BigDecimal("314.96"), cart.getTotalPrice());
        assertEquals(new BigDecimal("35.00"), cart.getTotalTax());

    }
    ////////////////// Step 2 Ends ///////////////////

    ////////////////// Step 3 Starts ///////////////////
    @Test
    public void testAddProducts_WithBuyXGetY_1() {
        // Given
        Product doveSoap = new Product("Dove Soap", BigDecimal.valueOf(39.99));
        Product axeDeos = new Product("Axe Deo", BigDecimal.valueOf(89.99));
        Map<Product, List<ICartItemDiscount>> discountsByProduct = Map.of(doveSoap,
                List.of(new BuyXGetYDiscount(2, 1)));

        ShoppingCart cart = new ShoppingCart();
        cart.setDiscountCalculator(new DiscountCalculator(discountsByProduct));
        cart.setTaxCalculator(new TaxCalculator(List.of(new SalesTax(12.5f))));

        // when
        cart.addItem(new CartItem(doveSoap, 3));

        // then

        assertEquals(3,
                cart.getItems().stream().filter(i -> i.getProduct().equals(doveSoap)).findFirst().get().getQuantity());

        assertEquals(new BigDecimal("89.98"), cart.getTotalPrice());
        assertEquals(new BigDecimal("39.99"), cart.getTotalDiscounts());
        assertEquals(new BigDecimal("10.00"), cart.getTotalTax());

    }

    @Test
    public void testAddProducts_WithBuyXGetY_2() {
        // Given
        Product doveSoap = new Product("Dove Soap", BigDecimal.valueOf(39.99));
        Product axeDeos = new Product("Axe Deo", BigDecimal.valueOf(89.99));
        Map<Product, List<ICartItemDiscount>> discountsByProduct = Map.of(doveSoap,
                List.of(new BuyXGetYDiscount(2, 1)));

        ShoppingCart cart = new ShoppingCart();
        cart.setDiscountCalculator(new DiscountCalculator(discountsByProduct));
        cart.setTaxCalculator(new TaxCalculator(List.of(new SalesTax(12.5f))));

        // when
        cart.addItem(new CartItem(doveSoap, 5));

        // then
        assertEquals(5,
                cart.getItems().stream().filter(i -> i.getProduct().equals(doveSoap)).findFirst().get().getQuantity());
        assertEquals(new BigDecimal("179.96"), cart.getTotalPrice());
        assertEquals(new BigDecimal("39.99"), cart.getTotalDiscounts());
        assertEquals(new BigDecimal("20.00"), cart.getTotalTax());

    }

    @Test
    public void testAddProducts_WithBuyXGetY_3() {
        // Given
        Product doveSoap = new Product("Dove Soap", BigDecimal.valueOf(39.99));
        Product axeDeos = new Product("Axe Deo", BigDecimal.valueOf(89.99));
        Map<Product, List<ICartItemDiscount>> discountsByProduct = Map.of(doveSoap,
                List.of(new BuyXGetYDiscount(2, 1)));

        ShoppingCart cart = new ShoppingCart();
        cart.setDiscountCalculator(new DiscountCalculator(discountsByProduct));
        cart.setTaxCalculator(new TaxCalculator(List.of(new SalesTax(12.5f))));

        // when
        cart.addItem(new CartItem(doveSoap, 3));
        cart.addItem(new CartItem(axeDeos, 2));

        // then
        assertEquals(3,
                cart.getItems().stream().filter(i -> i.getProduct().equals(doveSoap)).findFirst().get().getQuantity());
        assertEquals(2,
                cart.getItems().stream().filter(i -> i.getProduct().equals(axeDeos)).findFirst().get().getQuantity());
        assertEquals(new BigDecimal("292.46"), cart.getTotalPrice());
        assertEquals(new BigDecimal("39.99"), cart.getTotalDiscounts());
        assertEquals(new BigDecimal("32.50"), cart.getTotalTax());

    }

    ////////////////// Step 3 Ends ///////////////////
}
