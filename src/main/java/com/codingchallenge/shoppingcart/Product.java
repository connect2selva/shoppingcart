package com.codingchallenge.shoppingcart;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.UUID;

public class Product {
    private UUID id;
    private String name;
    private BigDecimal price;

    public Product(String name, BigDecimal price) {
        this.id = UUID.randomUUID();
        setName(name);
        setPrice(price);
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public int hashCode() {
        return this.getId().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        
        if (Objects.isNull(obj))
            return false;

        if (!(obj instanceof Product))
            return false;

        Product that = (Product) obj;

        return this.getId().equals(that.getId());
    }

}
