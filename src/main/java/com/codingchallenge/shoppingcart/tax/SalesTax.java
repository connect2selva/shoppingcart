package com.codingchallenge.shoppingcart.tax;

import java.math.BigDecimal;

public class SalesTax implements Tax {
    private float percentage;

    public SalesTax(float percentage) {
        this.percentage = percentage;
    }

    @Override
    public BigDecimal calculateTax(BigDecimal price) {
        return price.multiply(BigDecimal.valueOf(this.percentage / 100));
    }

}
