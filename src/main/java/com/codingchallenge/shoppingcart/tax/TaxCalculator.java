package com.codingchallenge.shoppingcart.tax;

import java.math.BigDecimal;
import java.util.List;

public class TaxCalculator implements ITaxCalculator {
    List<Tax> applicableTaxes;

    public TaxCalculator(List<Tax> applicableTaxes) {
        this.applicableTaxes = applicableTaxes;
    }

    @Override
    public BigDecimal compute(BigDecimal totalPrice) {
        return applicableTaxes.stream().map(t -> t.calculateTax(totalPrice)).reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}
