package com.codingchallenge.shoppingcart.tax;

import java.math.BigDecimal;

public interface ITaxCalculator {
    public BigDecimal compute(BigDecimal totalPrice);

}
