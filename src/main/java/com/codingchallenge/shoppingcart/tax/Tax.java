package com.codingchallenge.shoppingcart.tax;

import java.math.BigDecimal;

public interface Tax {
    public BigDecimal calculateTax(BigDecimal totalPrice);
}
