package com.codingchallenge.shoppingcart.discounts;

import java.math.BigDecimal;

import com.codingchallenge.shoppingcart.CartItem;

public interface ICartItemDiscount {
    public BigDecimal applyAndGetDiscount(CartItem cartItem);
}
