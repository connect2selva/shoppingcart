package com.codingchallenge.shoppingcart.discounts;

import java.math.BigDecimal;

import com.codingchallenge.shoppingcart.CartItem;

public class BuyXGetYDiscount implements ICartItemDiscount {
    private int x;
    private int y;

    public BuyXGetYDiscount(int x, int y) {
        this.x = x;
        this.y = y;

    }

    @Override
    public BigDecimal applyAndGetDiscount(CartItem cartItem) {
        int discount = findDiscount(cartItem.getQuantity());
        BigDecimal discountAmt = cartItem.getProduct().getPrice().multiply(BigDecimal.valueOf(discount));
        cartItem.setSalePrice(cartItem.getSalePrice().subtract(discountAmt));
        return discountAmt;
    }

    private int findDiscount(int origQunatity) {
        int r = origQunatity % (x + y);
        int n = (origQunatity - r) / (x + y);

        int discount = Math.max(0, r - x) + (n * y);

        return discount;
    }

}
