package com.codingchallenge.shoppingcart.discounts;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.codingchallenge.shoppingcart.Product;
import com.codingchallenge.shoppingcart.ShoppingCart;

public class DiscountCalculator implements IDiscountCalculator {
    Map<Product, List<ICartItemDiscount>> discountsByProduct;

    public DiscountCalculator(Map<Product, List<ICartItemDiscount>> discountsByProduct) {
        this.discountsByProduct = discountsByProduct;
    }

    @Override
    public BigDecimal compute(ShoppingCart cart) {
        return computeCartItemDiscounts(cart);
    }

    private BigDecimal computeCartItemDiscounts(ShoppingCart cart) {
        return cart.getItems().stream().map((item) -> {
            List<ICartItemDiscount> discounts = discountsByProduct.getOrDefault(item.getProduct(), List.of());
            BigDecimal discountAmt = discounts.stream().map(d -> d.applyAndGetDiscount(item)).reduce(BigDecimal.ZERO,
                    BigDecimal::add);

            return discountAmt;
        }).reduce(BigDecimal.ZERO, BigDecimal::add);

    }
}
