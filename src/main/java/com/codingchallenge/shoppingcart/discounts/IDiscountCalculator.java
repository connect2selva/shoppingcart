package com.codingchallenge.shoppingcart.discounts;

import java.math.BigDecimal;

import com.codingchallenge.shoppingcart.ShoppingCart;

public interface IDiscountCalculator {
    public BigDecimal compute(ShoppingCart cart);
}
