package com.codingchallenge.shoppingcart;

import java.math.BigDecimal;

public class CartItem {

    private Product product;

    private int quantity;

    private BigDecimal origPrice;
    
    private BigDecimal salePrice;

    public CartItem(Product product, int quantity) {
        setProduct(product);
        setQuantity(quantity);
        setOrigPrice(getProduct().getPrice().multiply(BigDecimal.valueOf(getQuantity())));
        setSalePrice(getOrigPrice());
    }

    public BigDecimal getOrigPrice() {
        return origPrice;
    }

    public void setOrigPrice(BigDecimal origPrice) {
        this.origPrice = origPrice;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(BigDecimal salePrice) {
        this.salePrice = salePrice;
    }

    public void incQuantity(int quantity) {
        this.quantity += quantity;
        setOrigPrice(getProduct().getPrice().multiply(BigDecimal.valueOf(getQuantity())));
        setSalePrice(getOrigPrice());
    }

}
