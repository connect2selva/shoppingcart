package com.codingchallenge.shoppingcart;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

import com.codingchallenge.shoppingcart.discounts.IDiscountCalculator;
import com.codingchallenge.shoppingcart.tax.ITaxCalculator;

public class ShoppingCart {
    Map<Product, CartItem> items;
    ITaxCalculator taxCalculator;
    IDiscountCalculator discountCalculator;
    
    private BigDecimal totalTax;
    private BigDecimal totalDiscounts;

    public ShoppingCart() {
        items = new LinkedHashMap<>();
    }

    public void addItem(CartItem cartItem) {
        CartItem existItem = items.get(cartItem.getProduct());

        if (Objects.nonNull(existItem)) {
            existItem.incQuantity(cartItem.getQuantity());
        } else {
            items.put(cartItem.getProduct(), cartItem);
        }
    }

    public Collection<CartItem> getItems() {
        return items.values();
    }

    public BigDecimal getTotalPrice() {
        BigDecimal price = items.values().stream().map(i -> i.getOrigPrice()).reduce(BigDecimal.ZERO, BigDecimal::add);
        
        if(discountCalculator != null) {
            this.totalDiscounts = discountCalculator.compute(this);
            price = price.subtract(totalDiscounts);
        }
        
        if (taxCalculator != null) {
            this.totalTax = taxCalculator.compute(price);
            price = price.add(this.totalTax);
        }
        
        return price.setScale(2, RoundingMode.HALF_UP);
    }
    
    public BigDecimal getTotalTax() {
        return totalTax.setScale(2, RoundingMode.HALF_UP);
    }

    public BigDecimal getTotalDiscounts() {
        return totalDiscounts;
    }
    
    public void setTaxCalculator(ITaxCalculator taxCalculator) {
        this.taxCalculator = taxCalculator;
    }

    public void setDiscountCalculator(IDiscountCalculator discountCalculator) {
        this.discountCalculator = discountCalculator;
    }
}
